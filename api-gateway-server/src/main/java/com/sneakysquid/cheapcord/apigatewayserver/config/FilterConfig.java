package com.sneakysquid.cheapcord.apigatewayserver.config;

import com.sneakysquid.cheapcord.apigatewayserver.filters.ZuulErrorFilter;
import com.sneakysquid.cheapcord.apigatewayserver.filters.ZuulPostFilter;
import com.sneakysquid.cheapcord.apigatewayserver.filters.ZuulPreFilter;
import com.sneakysquid.cheapcord.apigatewayserver.filters.ZuulRouteFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {

	@Bean
	public ZuulPreFilter preFilter() {
		return new ZuulPreFilter();
	}

	@Bean
	public ZuulPostFilter postFilter() {
		return new ZuulPostFilter();
	}

	@Bean
	public ZuulErrorFilter errorFilter() {
		return new ZuulErrorFilter();
	}

	@Bean
	public ZuulRouteFilter routeFilter() {
		return new ZuulRouteFilter();
	}

}
