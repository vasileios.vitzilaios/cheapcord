package com.sneakysquid.cheapcord.account.config;

import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

@Configuration
public class AccountConfig {

	@Bean
	public ModelMapper modelMapper() {

		var modelMapper = new ModelMapper();

		modelMapper.getConfiguration()
				.setSkipNullEnabled(true)
				.setPropertyCondition(Conditions.isNotNull());

		return modelMapper;
	}

	@Bean
	public Docket swaggerConfiguration() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.paths(PathSelectors.ant("/account/*"))
				.apis(RequestHandlerSelectors.basePackage("com.sneakysquid.cheapcord.account.controller"))
				.build()
				.apiInfo(apiDetails());
	}

	private ApiInfo apiDetails() {
		return new ApiInfo(
				"Account Microservice API",
				"API for Cheapcord",
				"0.0.1",
				"Free For All",
				new springfox.documentation.service.Contact("Vasilis Vitzilaios", "eimaigamatos.com", "eimaigamatos@gmail.com"),
				"API License",
				"licenseUrl.com",
				Collections.emptyList());
	}
}
