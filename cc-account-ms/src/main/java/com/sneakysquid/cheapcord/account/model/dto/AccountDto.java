package com.sneakysquid.cheapcord.account.model.dto;

import com.sneakysquid.cheapcord.account.support.validator.UniqueEmailConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccountDto {
	private Long id;
	private String objectId;
	private String firstName;
	private String lastName;
	private String username;
	private String usernameGen;
	private String password;
	@UniqueEmailConstraint
	private String email;
}
