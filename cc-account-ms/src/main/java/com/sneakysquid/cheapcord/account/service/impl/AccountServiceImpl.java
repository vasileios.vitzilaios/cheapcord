package com.sneakysquid.cheapcord.account.service.impl;

import com.sneakysquid.cheapcord.account.model.dto.AccountDto;
import com.sneakysquid.cheapcord.account.model.dto.AccountSearchParamsDto;
import com.sneakysquid.cheapcord.account.model.entity.Account;
import com.sneakysquid.cheapcord.account.repository.AccountRepository;
import com.sneakysquid.cheapcord.account.service.api.AccountService;
import com.sneakysquid.cheapcord.account.support.converter.AccountModelConverter;
import com.sneakysquid.cheapcord.account.support.service.AccountServiceSupport;
import com.sneakysquid.cheapcord.common.exception.account.AccountNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private AccountServiceSupport accountServiceSupport;

	@Override
	public AccountDto getAccountById(Long id) {

		var account = accountRepository.getAccountById(id)
				.orElseThrow(() -> new AccountNotFoundException("Account with id " + id + " does not exist"));

		return AccountModelConverter.entityToDto(account);
	}

	@Override
	public List<AccountDto> getAccountByUsernameGen(AccountSearchParamsDto accountSearchParamsDto) {

		String username = accountSearchParamsDto.getUsernameGen();
		List<Account> accounts = accountRepository.getAccountByUsernameGen(username)
				.orElseThrow(() -> new AccountNotFoundException("No account found match this name"));

		return AccountModelConverter.entityToDto(accounts);
	}

	@Override
	public List<AccountDto> getAllAccounts() {

		List<Account> accounts = accountRepository.findAll();

		return AccountModelConverter.entityToDto(accounts);
	}

	@Override
	public void deleteAccount(Long id) {
		accountRepository.deleteById(id);
	}

	@Override
	public AccountDto saveAccount(AccountDto accountDto) {

		var accountWithUsernameGen = accountServiceSupport.persistUsername(AccountModelConverter.dtoToEntity(accountDto));
		var account = accountRepository.save(accountWithUsernameGen);

		return AccountModelConverter.entityToDto(account);
	}

	@Override
	public AccountDto editAccount(AccountDto accountDto) {

		var account = accountRepository.save(AccountModelConverter.dtoToEntity(accountDto));

		return AccountModelConverter.entityToDto(account);
	}

}
