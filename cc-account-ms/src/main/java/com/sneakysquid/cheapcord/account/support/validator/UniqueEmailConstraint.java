package com.sneakysquid.cheapcord.account.support.validator;

import javax.validation.Constraint;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UniqueEmailValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
public @interface UniqueEmailConstraint {
	String message() default "An account with the selected email already exists";
	Class[] groups() default {};
	Class[] payload() default {};
}
