package com.sneakysquid.cheapcord.account.repository;

import com.sneakysquid.cheapcord.account.model.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

	Optional<Account> getAccountById(Long id);
	Optional<Account> getAccountByEmail(String email);
	@Query("select a from Account a where a.usernameGen like %?1%")
	Optional<List<Account>> getAccountByUsernameGen(String usernameGen);
}
