package com.sneakysquid.cheapcord.account.support.service;

import com.sneakysquid.cheapcord.account.model.entity.Account;
import com.sneakysquid.cheapcord.account.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

@Component
public class AccountServiceSupport {

	private final AccountRepository accountRepository;

	@Autowired
	public AccountServiceSupport(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}

	public Account persistUsername(Account account) {

		var accountWithUsernameGen = accountRepository.save(account);

		var sb = new StringBuilder(accountWithUsernameGen.getUsername());
		accountWithUsernameGen.setUsernameGen(sb.append("#").append(String.format("%04d", account.getId())).toString());

		return accountWithUsernameGen;
	}

	public boolean emailExists(String email) {

		return !ObjectUtils.isEmpty(accountRepository.getAccountByEmail(email));
	}
}
