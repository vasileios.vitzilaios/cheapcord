package com.sneakysquid.cheapcord.account.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name="account")
public class Account implements Serializable {

	@Id
	@GeneratedValue
	@Column
	private Long id;

	@NotBlank(message = "First name cannot be empty")
	@Column(name="first_name")
	private String firstName;

	@NotBlank(message = "Last name cannot be empty")
	@Column(name="last_name")
	private String lastName;

	@NotBlank(message = "User name cannot be empty")
	@Column(name="username")
	private String username;

	@Column(name="username_gen")
	private String usernameGen;

	@NotBlank(message = "Password cannot be empty")
	@Column(name="password")
	private String password;

	@Email
	@NotBlank(message = "Email cannot be empty")
	@Column(name="email")
	private String email;
}
