package com.sneakysquid.cheapcord.account.controller;

import com.sneakysquid.cheapcord.account.model.dto.AccountDto;
import com.sneakysquid.cheapcord.account.model.dto.AccountSearchParamsDto;
import com.sneakysquid.cheapcord.account.service.api.AccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.QueryParam;
import java.util.List;

@RestController
@RequestMapping(value = "/account")
@Api("Account MS main API")
public class AccountController {

	/******* GET Requests *******/

	@Autowired
	private AccountService accountService;

	@GetMapping("/getAccountById")
	@ApiOperation(value = "Retrieves an account by its unique identifier",
	response = AccountDto.class)
	public ResponseEntity<AccountDto> getAccountById(Long id) {

		return ResponseEntity.ok(accountService.getAccountById(id));
	}

	@GetMapping("/getAllAccounts")
	@ApiOperation("Retrieves a list of all existing accounts.")
	public ResponseEntity<List<AccountDto>> getAllAccounts() {

		return ResponseEntity.ok(accountService.getAllAccounts());
	}

	@GetMapping("/deleteAccount")
	@ApiOperation("Physically deletes an account.")
	public ResponseEntity<HttpStatus> deleteAccount(@QueryParam("id") Long id) {

		return ResponseEntity.noContent().build();
	}

	/******* POST Requests *******/

	@PostMapping("/getAccountByUsernameGen")
	@ApiOperation("Saves an account with its details.")
	public ResponseEntity<List<AccountDto>> getAccountByUsernameGen(@RequestBody AccountSearchParamsDto account) {

		return ResponseEntity.ok(accountService.getAccountByUsernameGen(account));
	}

	@PostMapping("/saveAccount")
	@ApiOperation("Saves an account with its details.")
	public ResponseEntity<AccountDto> saveAccount(@RequestBody @Validated AccountDto account) {

		return ResponseEntity.ok(accountService.saveAccount(account));
	}

	@PostMapping("/editAccount")
	@ApiOperation("Edits account details")
	public ResponseEntity<AccountDto> editAccount(@RequestBody AccountDto account) {

		return ResponseEntity.ok(accountService.editAccount(account));
	}

	@PostMapping("/deleteAccount")
	@ApiOperation("Edits account details")
	public ResponseEntity<?> deleteAccount(@RequestBody AccountDto account) {
		accountService.deleteAccount(account.getId());
		return ResponseEntity.ok("Account with username " + account.getUsername() + " safely deleted");
	}

}
