package com.sneakysquid.cheapcord.account.support.validator;

import com.sneakysquid.cheapcord.account.support.service.AccountServiceSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueEmailValidator implements ConstraintValidator<UniqueEmailConstraint, String> {

	@Autowired
	private AccountServiceSupport accountServiceSupport;

	@Override
	public void initialize(UniqueEmailConstraint constraintAnnotation) {
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return !ObjectUtils.isEmpty(value) && !accountServiceSupport.emailExists(value);
	}
}
