package com.sneakysquid.cheapcord.account.service.api;

import com.sneakysquid.cheapcord.account.model.dto.AccountDto;
import com.sneakysquid.cheapcord.account.model.dto.AccountSearchParamsDto;

import java.util.List;

public interface AccountService {

	AccountDto getAccountById(Long id);
	List<AccountDto> getAccountByUsernameGen(AccountSearchParamsDto dto);
	List<AccountDto> getAllAccounts();
	void deleteAccount(Long id);
	AccountDto saveAccount(AccountDto dto);
	AccountDto editAccount(AccountDto dto);
}
