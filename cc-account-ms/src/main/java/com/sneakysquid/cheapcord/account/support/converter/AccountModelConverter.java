package com.sneakysquid.cheapcord.account.support.converter;

import com.sneakysquid.cheapcord.account.model.dto.AccountDto;
import com.sneakysquid.cheapcord.account.model.entity.Account;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class AccountModelConverter {

	private static ModelMapper modelMapper;

	@Autowired
	private AccountModelConverter(ModelMapper modelMapper) {
		setModelMapper(modelMapper);
	}

	public static Account dtoToEntity(AccountDto dto) {

		return modelMapper.map(dto, Account.class);
	}

	public static List<Account> dtoToEntity(List<AccountDto> dtoList) {

		return dtoList.stream()
				.map(AccountModelConverter::dtoToEntity)
				.collect(Collectors.toList());
	}

	public static AccountDto entityToDto(Account entity) {

		if (ObjectUtils.isEmpty(entity)) {
			return null;
		}

		return modelMapper.map(entity, AccountDto.class);
	}

	public static List<AccountDto> entityToDto(List<Account> entityList) {

		if (ObjectUtils.isEmpty(entityList)) {
			return new ArrayList<>();
		}

		return entityList.stream()
				.map(AccountModelConverter::entityToDto)
				.collect(Collectors.toList());
	}

	private static void setModelMapper(ModelMapper modelMapper) {
		AccountModelConverter.modelMapper = modelMapper;
	}
}
