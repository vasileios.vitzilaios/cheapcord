package com.sneakysquid.cheapcord.authentication.service.api;

import org.springframework.http.HttpEntity;
import org.springframework.util.MultiValueMap;

public interface AuthenticationService {

	HttpEntity<MultiValueMap<String, String>> getAccessToken(String code);

}
