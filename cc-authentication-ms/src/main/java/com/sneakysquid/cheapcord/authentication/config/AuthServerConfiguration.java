package com.sneakysquid.cheapcord.authentication.config;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AuthServerConfiguration extends AuthorizationServerConfigurerAdapter {

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {

		clients.inMemory()
				.withClient("cid")
				.secret(passwordEncoder().encode("secret"))
				.scopes("read", "write")
				.authorizedGrantTypes("authorization_code", "refresh_token")
				.redirectUris("http://localhost:8082/getAccessToken")
				.autoApprove(true);
	}

	/*@Bean
	JwtTokenStore getAccessTokenConverter() {
		return new JwtTokenStore(JwtTokenEnhancer.getInstance());
	}*/

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
		return restTemplateBuilder.build();
	}

}
