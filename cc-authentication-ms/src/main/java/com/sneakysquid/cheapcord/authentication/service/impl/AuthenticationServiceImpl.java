package com.sneakysquid.cheapcord.authentication.service.impl;

import com.sneakysquid.cheapcord.authentication.service.api.AuthenticationService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

	@Override
	public HttpEntity<MultiValueMap<String, String>> getAccessToken(String code) {


		var headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.set("authorization", "Basic Y2lkOnNlY3JldA==");

		MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
		map.add("grant_type","authorization_code");
		map.add("client_Id","cid");
		map.add("code",code);

		return new HttpEntity<>(map, headers);
	}

	public String getJwt(String username) {
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList("ROLE_USER");

		String token = Jwts
				.builder()
				.setId(UUID.randomUUID().toString())
				.setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512, "SneakySquid".getBytes()).compact();

		return "Bearer " + token;
	}
}
