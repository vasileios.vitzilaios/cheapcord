package com.sneakysquid.cheapcord.authentication.controller;

import com.sneakysquid.cheapcord.authentication.service.api.AuthenticationService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.ws.rs.QueryParam;

@RestController
@Api("Authentication MS main API")
public class AuthenticationController {

	@Autowired
	AuthenticationService authenticationService;

	@Autowired
	RestTemplate restTemplate;

	@GetMapping("/getAccessToken")
	public ResponseEntity<String> getAccessToken(@QueryParam("code") String code) {

		HttpEntity<MultiValueMap<String, String>> entity = authenticationService.getAccessToken(code);

		return restTemplate.exchange("http://localhost:8082/oauth/token",
				HttpMethod.POST,
				entity,
				String.class);
	}

}
