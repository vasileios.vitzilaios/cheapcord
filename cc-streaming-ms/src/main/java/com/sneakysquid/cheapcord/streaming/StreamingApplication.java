package com.sneakysquid.cheapcord.streaming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication(scanBasePackages = {"com.sneakysquid"})
@EnableEurekaClient
public class StreamingApplication {
	public static void main(String[] args) {
		SpringApplication.run(StreamingApplication.class, args);
	}
}
