package com.sneakysquid.cheapcord.common.exception.account;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.FieldError;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiError {

	private HttpStatus status;
	private int code;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	private String message;
	private String stackTrace;
	private LocalDateTime timestamp;
	private List<ApiSubError> subErrors;

	public ResponseEntity<ApiError> buildApiErrorResponse() {

		return ResponseEntity.status(this.getStatus().value()).body(this);
	}

	private record ApiSubError(String object, String field, String rejectedValue, String message) {}

	public static class builder {

		private HttpStatus status;
		private int code;
		private String message;
		private String stackTrace;
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
		private LocalDateTime timestamp;
		private List<ApiSubError> subErrors;

		public builder status(HttpStatus status) {
			this.status = status;
			return this;
		}

		public builder code(int code) {
			this.code = code;
			return this;
		}

		public builder message(String message) {
			this.message = message;
			return this;
		}

		public builder stackTrace(String stackTrace) {
			this.stackTrace = stackTrace;
			return this;
		}

		public builder timestamp(LocalDateTime timestamp) {
			this.timestamp = timestamp;
			return this;
		}

		public builder subErrors(List<FieldError> fieldErrors) {
			Assert.notNull(fieldErrors, "Object is null");
			List<ApiSubError> sb = new ArrayList<>();
			for (FieldError error : fieldErrors) {
				sb.add(new ApiSubError(error.getObjectName(),
						error.getField(),
						Objects.requireNonNull(error.getRejectedValue()).toString(),
						error.getDefaultMessage()));
			}
			this.subErrors = sb;

			return this;
		}

		public ApiError build() {

			var apiError = new ApiError();

			apiError.setStatus(status);
			apiError.setCode(code);
			apiError.setMessage(ObjectUtils.isEmpty(message) ? "" : message);
			apiError.setStackTrace(ObjectUtils.isEmpty(stackTrace) ? "" : stackTrace);
			apiError.setTimestamp(ObjectUtils.isEmpty(timestamp) ? LocalDateTime.now() : timestamp);
			apiError.setSubErrors(ObjectUtils.isEmpty(subErrors) ? new ArrayList<>() : subErrors);

			return apiError;
		}
	}

}
