package com.sneakysquid.cheapcord.common.util;

import org.springframework.util.ObjectUtils;

public class ErrorUtils {

	public static Throwable getCause(Throwable t) {
		if (ObjectUtils.isEmpty(t)) {
			return null;
		}
		return t.getCause();
	}

	public static String getCauseMessage(Throwable t) {
		if (!ObjectUtils.isEmpty(t)) {
			if (ObjectUtils.isEmpty(t.getMessage())) {
				return "Unknown Error";
			}
			return null;
		}
		return t.getMessage();
	}

	public static String getStackTrace(Throwable t) {
		return null;
	}
}
