package com.sneakysquid.cheapcord.common.exception.account;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ApiError> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {

		var apiError = new ApiError.builder()
				.status(HttpStatus.BAD_REQUEST)
				.code(HttpStatus.BAD_REQUEST.value())
				.message(e.getMessage())
				.stackTrace(e.getLocalizedMessage())
				.subErrors(e.getBindingResult().getFieldErrors())
				.build();

		return apiError.buildApiErrorResponse();
	}

	@ExceptionHandler(AccountNotFoundException.class)
	public ResponseEntity<ApiError> handleEntityNotFoundException(AccountNotFoundException e) {

		var apiError = new ApiError.builder()
				.status(HttpStatus.NOT_FOUND)
				.code(HttpStatus.NOT_FOUND.value())
				.message(e.getMessage())
				.stackTrace(e.getLocalizedMessage())
				.build();

		return apiError.buildApiErrorResponse();
	}

}
