package com.sneakysquid.cheapcord.common.exception.account;

import lombok.AllArgsConstructor;

import javax.persistence.EntityNotFoundException;

@AllArgsConstructor
public class AccountNotFoundException extends EntityNotFoundException {

	public AccountNotFoundException(String message) {
		super(message);
	}

}
